#include <string>
#include <iostream>
#include "sqlite3.h"
#include <sstream>

#define ARR_SIZE 3
#define BALANCE_ARR_SIZE 2

using std::cout;
using std::string;
using std::endl;
using std::stringstream;

bool errorIdentifier(int error, char* zErrMsg);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int callback(void* notUsed, int argc, char** argv, char** azCol);
void purchaseMaker(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
int getBalance(void* notUsed, int argc, char** argv, char** azCol);
void resetArr(string * arr, int size);
//bonus:
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
int getCarPrice(void * notUsed, int argc, char ** argv, char ** azCol);
int printBuyers(void * notUsed, int argc, char ** argv, char ** azCol);

string arr[ARR_SIZE];
string balanceArr[BALANCE_ARR_SIZE];
int carPrice;

int main()
{
	int error = 0;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag, transferFlag;

	error = sqlite3_open("carsDealer.db", &db);

	flag = carPurchase(10, 1, db, zErrMsg);
	if (flag)
	{
		cout << "Purchase completed!" << endl;
	}
	else
	{
		cout << "Something went wrong....." << endl;
	}
	flag = carPurchase(8, 10, db, zErrMsg);
	if (flag)
	{
		cout << "Purchase completed!" << endl;
	}
	else
	{
		cout << "Something went wrong....." << endl;
	}
	flag = carPurchase(11, 17, db, zErrMsg);
	if (flag)
	{
		cout << "Purchase completed!" << endl;
	}
	else
	{
		cout << "Something went wrong....." << endl;
	}

	transferFlag = balanceTransfer(8, 5, 10000, db, zErrMsg);
	if (transferFlag)
	{
		cout << "Transfer Accomplished!" << endl;
	}
	else
	{
		cout << "You can't transfer the money..." << endl;
	}

	whoCanBuy(16, db, zErrMsg);

	sqlite3_close(db);
	system("pause");
	return 0;
}

//check if there's an error, return True if error does exists
bool errorIdentifier(int error, char * zErrMsg)
{
	if (error != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return true;
	}
	return false;
}

bool carPurchase(int buyerid, int carid, sqlite3 * db, char * zErrMsg)
{
	int rc = 0;
	stringstream msg;

	//check if car is available - arr[0]
	msg << "select available from cars where id = " << carid << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return 1;
	}
	msg.str(string());

	//check car's price - arr[1]
	msg << "select price from cars where id = " << carid << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return 1;
	}
	msg.str(string());

	//check buyer's balance - arr[2]
	msg << "select balance from accounts where Buyer_id = " << buyerid << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return 1;
	}
	msg.str(string());

	if ( atoi(arr[1].c_str()) < atoi(arr[2].c_str()) && arr[0] != "0") //if buyer's balance is bigger than car price
	{
		cout << "CAN" << endl;
		purchaseMaker(buyerid, carid, db, zErrMsg);
		return true;
	}
	cout << "CAN NOT" << endl;
	resetArr(arr, ARR_SIZE);
	return false;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	static int i = 0;
	arr[i % ARR_SIZE] = argv[0];
	i++;
	return 0;
}

void purchaseMaker(int buyerid, int carid, sqlite3 * db, char * zErrMsg)
{
	stringstream msg;
	int rc = 0;
	int newCustomerBalance = atoi(arr[2].c_str()) - atoi(arr[1].c_str());

	//starting transaction
	msg << "begin transaction;"; 
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());

	//changing customers balance 
	msg << "update accounts set balance = " << newCustomerBalance << " where buyer_id = " << buyerid << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());

	//changing car to not available
	msg << "update cars set available = 0 where id = " << carid << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());

	//end transaction
	msg << "commit;";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());
}

bool balanceTransfer(int from, int to, int amount, sqlite3 * db, char * zErrMsg)
{
	stringstream msg;
	int rc = 0;
	int balance_From = 0, balance_To = 0;
	int newBalance_From = 0;
	int newBalance_To = 0;

	resetArr(balanceArr, BALANCE_ARR_SIZE);

	//starting transaction
	msg << "begin transaction;";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	msg.str(string());

	//check the balance of the "from" account
	msg << "select balance from accounts where Buyer_id = " << from << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), getBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		msg << "rollback;";
		rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
		return false;
	}
	msg.str(string());

	//check the balance of the "to" account
	msg << "select balance from accounts where Buyer_id = " << to << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), getBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		msg << "rollback;";
		rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
		return false;
	}
	msg.str(string());

	newBalance_From = atoi(balanceArr[0].c_str()) - amount;
	if (newBalance_From < 0) //can't have minus
	{
		msg << "rollback;";
		rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
		return false;
	}
	newBalance_To = atoi(balanceArr[1].c_str()) + amount;
	
	//updating the "from" account balance
	msg << "update accounts set balance = " << newBalance_From << " where id = " << from << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		msg << "rollback;";
		rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
		return false;
	}
	msg.str(string());

	//updating the "to" account balance
	msg << "update accounts set balance = " << newBalance_To << " where id = " << to << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		msg << "rollback;";
		rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
		return false;
	}
	msg.str(string());

	//end transaction
	msg << "commit;";
	rc = sqlite3_exec(db, (msg.str()).c_str(), 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	msg.str(string());

	return true;
}

int getBalance(void * notUsed, int argc, char ** argv, char ** azCol)
{
	static int i = 0;
	balanceArr[i % BALANCE_ARR_SIZE] = argv[0];
	i++;
	return 0;
}

void resetArr(string * arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = "";
	}
}

//BUNOS:

void whoCanBuy(int carId, sqlite3 * db, char * zErrMsg)
{
	int rc = 0;
	stringstream msg;

	//get car price using his ID
	msg << "select price from cars where id = " << carId << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), getCarPrice, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());

	//check who can buy the car
	msg << "select id from accounts group by id having balance >= " << carPrice << ";";
	rc = sqlite3_exec(db, (msg.str()).c_str(), printBuyers, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	msg.str(string());
}

int getCarPrice(void * notUsed, int argc, char ** argv, char ** azCol)
{
	carPrice = atoi(argv[0]);
	return 0;
}

int printBuyers(void * notUsed, int argc, char ** argv, char ** azCol)
{
	cout << "The customer with ID number " << argv[0] << " can buy the car." << endl;
	return 0;
}

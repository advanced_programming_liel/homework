#include <string>
#include <iostream>
#include "sqlite3.h"

using std::cout;
using std::string;
using std::endl;

bool errorIdentifier(int error, char* zErrMsg);

int main()
{
	int error = 0;
	sqlite3* db;
	char *zErrMsg = 0;
	
	//open database
	error = sqlite3_open("FirstPart.db", &db);
	if (error)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	//create table
	error = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string);", NULL, 0, &zErrMsg);
	if (errorIdentifier(error, zErrMsg))
	{
		return 1;
	}
	
	//add 3 students
	error = sqlite3_exec(db, "insert into people(name) values(\"Liel\");", NULL, 0, &zErrMsg);
	if (errorIdentifier(error, zErrMsg))
	{
		return 1;
	}
	error = sqlite3_exec(db, "insert into people(name) values(\"Aaron\");", NULL, 0, &zErrMsg);
	if (errorIdentifier(error, zErrMsg))
	{
		return 1;
	}
	error = sqlite3_exec(db, "insert into people(name) values(\"Evyatar\");", NULL, 0, &zErrMsg);
	if (errorIdentifier(error, zErrMsg))
	{
		return 1;
	}

	//change student name
	error = sqlite3_exec(db, "update people set name = \"Sagi\" where name = \"Aaron\";", NULL, 0, &zErrMsg);
	if (errorIdentifier(error, zErrMsg))
	{
		return 1;
	}

	system("pause");
	sqlite3_close(db);
	return 0;
}

//check if there's an error, return True if error does exists
bool errorIdentifier(int error, char * zErrMsg)
{
	if (error != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return true;
	}
	return false;
}
